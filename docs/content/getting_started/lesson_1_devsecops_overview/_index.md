---
bookCollapseSection: false
weight: 30
---

# Introduction to DevSecOps

This section provides a quick introduction to DevSecOps, how it fits into the Software Development Life Cycle (SDLC), and it's benefits. This goes over why implementing DevSecOps is crucial to every organization.

## What is DevSecOps

DevSecOps is the process of integrating security into every part of the Software Development Life Cycle (SDLC). DevSecOps follows the idea of [Shifting Left](https://www.aquasec.com/cloud-native-academy/devsecops/shift-left-devops/), meaning that vulnerabilities are caught and handled earlier in the SDLC rather than later. This not only saves the time (since there is a great cost in refactoring), but also makes sure vulnerabilities are handled before they become a major problem.

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/cicd.png)

Within a standard CI/CD pipeline (as seen above), there are the following stages:

- **Plan & Create**: Planning epics, milestones, and issues. Adding code to the repo.
- **Integrate & Verify**: Running tests and security scans to make sure application will run properly.
- **Deploy & Operate**: Deploying application to different environments.
- **Monitor & Improve**: Gather data on the deployed application to make changes where needed.

DevSecOps can be seen in the following ways:

- Every time code is committed, it is scanned in order to find vulnerabilities
- Vulnerabilities are displayed along with detailed information, including the solution
- If a vulnerability is found, a merge can be blocked and require approval by security team
- Issues can be created from vulnerabilities
- Vulnerabilities found in the main branch can be triaged

## What are the Benefits of DevSecOps

DevSecOps allows you to detect and resolve vulnerabilities before deploying your application to production. Catching security issues early in the SDLC can:

- Reduce development and maintenance costs
- Prevent harm to your company’s reputation
- Increase developer efficiency
- Enhance collaboration between security team and developers

---

Congratulations, you can now see the value of implementing DevSecOps! Now let's move on to installing all
the prerequisites for this tutorial.

{{< button relref="/" >}}Go Home{{< /button >}}
{{< button relref="/lesson_2_tutorial_prerequisites" >}}Next Lesson{{< /button >}}