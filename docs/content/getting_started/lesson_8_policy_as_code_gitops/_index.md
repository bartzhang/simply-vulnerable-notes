---
bookCollapseSection: false
weight: 100
---

# Policy as Code with GitOps (Optional)

[GitOps](https://about.gitlab.com/topics/gitops/) is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.

In this section we will take a look at how GitLab can listen to a folder and apply changes as they are committed using GitOps.

## Step 1: Enabling Policy as Code

Policy as Code is a way of creating policies by just editing code. In this section we will be using [GitOps](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html) in order to deploy network policies which will limit access to our **restricted-echo** pods from other pods.

We can see the Network Policy we will be applying in the [network-policies/restricted-echo.yaml file](/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/blob/main/other/network-policies/restricted-nginx-policy.yaml), which prevents any pod without the label `access=true` from accessing the `restricted-echo` pod.

1. Open the **WebIDE** from the project page

{{< hint info >}}
**Note:** To learn more about the GitLab Web IDE and how to use/configure it, checkout the
[Web IDE documentation](https://docs.gitlab.com/ee/user/project/web_ide/)
{{< /hint >}}

2. Open up [**.gitlab > agents > simplenotes > config.yaml**](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/blob/main/.gitlab/agents/simplenotes/config.yaml)

3. Add the following to the file, updating the `<your-full-project-path>`:
```
gitops:
  manifest_projects:
  - id: <your-full-project-path>
    paths:
    - glob: '/network-policies/*.yaml'
```

It looks as follows for this particular project:
```
gitops:
  manifest_projects:
  - id: gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes
    paths:
    - glob: '/network-policies/*.yaml'
```

4. Click on the **Source Control** Tab in the side-panel of the WebIDE. It is the [3rd icon from the top](https://code.visualstudio.com/docs/sourcecontrol/overview#_commit)

5. Click on the **Commit to 'main'** Button

Now let's wait for the pipeline to complete, this should take a few mins. If the pipeline happens to fail, please checkout the [troubleshooting documentation](../../documentation/troubleshooting).

### Step 2: Testing Policy as Code

1. Open a terminal and connect to your cluster

```bash
$ gcloud container clusters get-credentials fern-notes --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-notes.
```

{{< hint info >}}
**Note:** You should use the command, provided by GKE as seen in previous lessons
{{< /hint >}}

2. Verify that the network policy has been installed

```bash
$ kubectl get networkpolicy -n notes-app

NAME                      POD-SELECTOR           AGE
access-restricted-nginx   app=restricted-nginx   35m
```

3. Create a `busybox` container and try and access our `restricted-echo` pod. When performing **wget**, we should timeout since we cannot access the pod without the label `access=true`

```bash
$ kubectl run busybox -n notes-app --rm -ti --image=busybox:1.28 -- /bin/sh

  If you don't see a command prompt, try pressing enter.
  / # wget --spider --timeout=1 restricted-nginx

  Connecting to restricted-nginx (10.4.5.28:80)
  wget: download timed out
```

4. Exit from the container by typing `exit` in the command-line

5.  Create a `busybox` container, with the `access=true` label, and try and access our `restricted-echo` pod. This time **wget** should work since we have the label present!

```bash
$ kubectl run busybox -n notes-app --rm -ti --labels="access=true" --image=busybox:1.28 -- /bin/sh

  If you don't see a command prompt, try pressing enter.
  / # wget --spider --timeout=1 restricted-nginx

  Connecting to restricted-nginx (10.4.5.28:80)
  remote file exists
```

---

Congratulations, you are now able to use GitOps to deploy policy as code! You can go to the next section to see what's next in terms of mastering GitLab security.

{{< button relref="/lesson_7_branch_protection_and_additional_scanner_configuration" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_9_looking_forward" >}}Next Lesson{{< /button >}}