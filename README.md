# Simple Notes (Security Demo Application) 🔒🔑🔑

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

This application is used for taking simple notes, it has been created to showcase many of GitLab's security features and provide a tutorial around how to use them. Get started by viewing the [**Tutorial Documentation**](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/).

## Lessons

This tutorial contains the following lessons:

- [Lesson 1: DevSecOps Overview](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_1_devsecops_overview/)  
- [Lesson 2: Tutorial Prerequisites](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_2_tutorial_prerequisites/)  
- [Lesson 3: Deploying the Demo Application](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_3_deploying_the_demo_application/)  
- [Lesson 4: Setting up and Configuring the Security Scanners and Policies](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_4_setting_up_and_configuring_the_security_scanners_and_policies/)  
- [Lesson 5: Developer Workflow](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_5_developer_workflow/)  
- [Lesson 6: AppSec Workflow](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_6_appsec_workflow/)  

The below are optional, but very useful for a deeper understanding of what is available with GitLab in terms of security:

- [Lesson 7: Branch Protection and Additional Scanner Configuration (Optional)](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_7_branch_protection_and_additional_scanner_configuration)  
- [Lesson 8: Policy as Code with GitOps (Optional)](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_8_policy_as_code_gitops/)  
- [Lesson 9: Looking Forward (Optional)](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_9_looking_forward/)  

## Learning Outcomes

By completing this tutorial you will learn the following:

- How to achieve comprehensive security scanning without adding a bunch of new tools and processes
- How to secure your cloud native applications and IaC environments within existing DevOps workflows
- How to use a single-source-of-truth to improve collaboration between dev and sec
- How to manage all of your software vulnerabilities in one place
- How to automate and monitor your security policies and simplify auditing
- How to detect unknown vulnerabilities and errors using fuzz-testing
- How to configure the security scanners and make them run on a schedule
- How to enable separation of duties and adhere to compliance
- How to perform GitOps and deploy to a Kubernetes cluster

## Additional Resources

- [Security and Governance Demos](https://gitlab.com/gitlab-da/tutorials/security-and-governance)
- [GitLab Ultimate Pricing Page](https://about.gitlab.com/pricing/ultimate/)
- [DevOps Solution Resource - DevSecOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/)
- [What is DevSecOps?](https://about.gitlab.com/topics/devsecops/)
- [Integrating Security into your DevOps workflow](https://about.gitlab.com/solutions/dev-sec-ops/)
- [GitLab Secure Direction Page](https://about.gitlab.com/direction/secure/)
- [GitLab Govern Direction Page](https://about.gitlab.com/direction/govern/)

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)