FROM python:3.10-bookworm

RUN apt install wget gcc git gpg openssl -y

RUN wget https://r.mariadb.com/downloads/mariadb_repo_setup
RUN chmod +x mariadb_repo_setup
RUN ./mariadb_repo_setup

#RUN apt-get update -y

RUN apt install libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev -y

RUN pip3 install --upgrade pip
RUN pip3 install packaging

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app

EXPOSE 3306/tcp
EXPOSE 5000/tcp
ENTRYPOINT [ "python" ]
CMD [ "run.py" ]
